section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
        .loop:
            cmp byte[rdi + rax], 0          ; Checks if this symbol is a null terminated    
            je .end                         ; Checks ZF flag: if ZF=1 =>end                   
            inc rax                     
            jmp .loop
        .end:    
            ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
        call string_length                   ; The line length
        mov rsi, rdi                         ; The line address which we will output
        mov rdx, rax                         
        mov rdi, 1                           ; stdout descriptor
        mov rax, 1                           ; write syscall number
        syscall 
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
        push rdi                        
        mov rsi, rsp
        pop rdi                    
        mov rax, 1                     
        mov rdx, 1                      
        mov rdi, 1                        
        syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    push rcx
    call print_char
    pop rcx
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
        xor r8, r8
        xor rdx, rdx
        mov r9, 10          
        mov rax, rdi 
        mov r8, 1 
        dec rsp
        mov byte [rsp], 0

        .loop:
            xor rdx, rdx 
            div r9          
            mov rsi, rdx
            add rsi, 48         ;ASCII
            inc r8
            dec rsp
            mov [rsp], sil
            cmp rax, 0
            jne .loop
        mov rdi, rsp
        push rsi
        push rcx
        call print_string       ;result
        pop rcx
        pop rsi
        add rsp, r8    
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    xor rdx, rdx
        mov rax, rdi
        cmp rax, 0
        jge .print
        push rax
        push rdx
        push rcx
        mov rdi, '-'
        call print_char 
        pop rcx 
        pop rdx
        pop rax
        neg rax
        .print:
            mov rdi, rax
            push rcx
            call print_uint
            pop rcx
            ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax, 1
        mov r8, rdi                 ;first line
        mov r9, rsi                 ;second line
        xor rcx, rcx
        xor rdi, rdi
        xor rsi, rsi
        .loop:
            mov dil, [r8 + rcx]
            mov sil, [r9 + rcx]
            cmp sil, dil
            jne .different
            cmp byte dil, 0
            je .end
            inc rcx
            jmp .loop

        .different:
            mov rax, 0
        .end: 
            ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
        mov rdx, 1
        mov rdi, 0
        mov rax, 0
        dec rsp
        mov rsi, rsp
        syscall
        cmp rax, 0
        je .null
        mov rax, [rsp]
        inc rsp
        
    ret 
        .null:
            mov rax, 0
            inc rsp
            ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
        mov r8, rdi
    mov r9, rsi

    .skip_whitespaces:          
        call read_char
        cmp al, 0x20
        je .skip_whitespaces
        cmp al, 0x9
        je .skip_whitespaces
        cmp al, 0xA
    xor rdx, rdx                ;counter
    jmp .write
    .loop:                      
        push rdx
        call read_char
        pop rdx
    .write:                     ; Checks the end of the line
        cmp al, 0xA             
        je .end
        cmp al, 0x20
        je .end
        cmp al, 4
        je .end 
        cmp al, 0x9
        je .end
        cmp al, 0
        je .end
        inc rdx
        cmp rdx, r9
        jge .overflow
        dec rdx
        mov [r8+rdx], al
        inc rdx
        jmp .loop
    .end:                       
        mov byte[r8+rdx],0
        mov rax, r8
        ret
    .overflow:                  
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    mov r10, 10
    xor r9, r9
    xor r8, r8
    .loop: 
        mov r8b, [rdi + r9]
        cmp r8b, '0'
        jl .end
        cmp r8b, '9'
        jg .end                     
        sub r8b, '0'                
        mul r10                     
        add rax, r8                 
        inc r9                      
        jmp .loop
    .end:
        mov rdx, r9
        ret
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    cmp rdx, 0
    je .end
    inc rdx
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r9, r9
    xor rcx, rcx
    push rdi
    call string_length
    pop rdi
    cmp rax, rdx
    jle .loop
    mov rax, 0
    ret
   
    .loop: 
        mov r9b, [rdi + rcx] 
        mov [rsi + rcx], r9b 
        inc rcx 
        cmp byte[rsi + rcx], 0
        jne .loop
    mov rax, rdx
    ret
